#!/usr/bin/env bash

set -eu

year=$1
day=$2
binary=$3

for folder in 1962428 2904152 2966467 2982151 3903573 3970440 3973932 3973972 4003171 4003173 4669613 4762696 4763282 4802244 4814701 4886535 4902055
do
    input="/home/pgagelin/workspace/aoc-data/$folder/$year/$day.in"
    answers="/home/pgagelin/workspace/aoc-data/$folder/$year/$day.ok"
    [ ! -f "$input" ] && echo "Missing input for folder=$folder" && continue
    [ ! -f "$answers" ] && echo "Missing answers for folder=$folder" && continue

    expected=$(cat "$answers")
    actual=$("$binary" "$input")
    [ "$expected" = "$actual" ] && echo "Correct answers for folder=$folder" || echo "Incorrect answers for folder=$folder"
done
