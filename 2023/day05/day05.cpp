//
// Day 5: If You Give A Seed A Fertilizer
//
// https://adventofcode.com/2023/day/5
//

#include <algorithm>
#include <fstream>
#include <iostream>
#include <limits>
#include <sstream>
#include <vector>

struct Range {
   bool overlap(const Range& range) const { return range.src <= src - 1 + len && range.src - 1 + range.len >= src; }
   Range destination(const Range& range) const
   {
      Range i;
      i.src = std::max(src, range.src);
      i.dst = -1;
      i.len = std::min(len - (i.src - src), range.len - (i.src - range.src));
      i.src = dst + (i.src - src);
      return i;
   }
   long src;
   long dst;
   long len;
};

struct Map {
   void transform(std::vector<Range>& sources) const
   {
      std::vector<Range> destinations;
      for (const Range& source : sources)
         for (const Range& range : ranges)
            if (range.overlap(source))
               destinations.emplace_back(range.destination(source));
      std::swap(sources, destinations);
   }

   void initialize(std::vector<Range>& r)
   {
      ranges.clear();
      std::copy_n(r.begin(), r.size(), std::back_inserter(ranges));
      std::sort(ranges.begin(), ranges.end(), [](const Range& a, const Range& b) { return a.src < b.src; });

      // Fill holes between ranges with identity association
      std::vector<Range> missings;
      for (size_t i = 0; i + 1 < ranges.size(); ++i)
         if (ranges[i].src + ranges[i].len != ranges[i + 1].src)
            missings.emplace_back(Range(ranges[i].src + ranges[i].len, ranges[i].src + ranges[i].len, ranges[i + 1].src - (ranges[i].src + ranges[i].len)));
      std::copy_n(missings.begin(), missings.size(), std::back_inserter(ranges));
      std::sort(ranges.begin(), ranges.end(), [](const Range& a, const Range& b) { return a.src < b.src; });

      // Ranges are now contiguous, with two additional ranges we map [0, max]
      if (ranges.front().src > 0)
         ranges.emplace(ranges.begin(), Range(0, 0, ranges.front().src));
      const long last = ranges.back().src + ranges.back().len;
      ranges.emplace_back(Range(last, last, std::numeric_limits<long>::max() - last + 1));

      r.clear();
   }

   std::vector<Range> ranges;
};

int main(int argc, char **argv)
{
   std::ifstream file(argc == 2 ? argv[1] : "2023/day05/test01.txt");
   if (!file.is_open())
      return std::cerr << "Failed to open input file\n", EXIT_FAILURE;

   // Parse seeds on the first line
   std::vector<long> seeds;
   std::string line;
   std::getline(file, line);
   const std::string_view seed = "seeds: ";
   if (!line.starts_with(seed))
      std::cerr << "Failed to parse invalid input\n";
   line.erase(0, seed.length());
   std::stringstream s(line);
   while (s >> seeds.emplace_back(0)) {}
   seeds.pop_back();

   // Parse ranges
   Map soils, fertilizers, waters, lights, temperatures, humidities, locations;
   const auto parse = [](std::vector<Range>& v, const std::string& l) {
      Range& range = v.emplace_back();
      std::stringstream s(l);
      s >> range.dst;
      s >> range.src;
      s >> range.len;
   };
   std::vector<Range> ranges;
   std::getline(file, line);  // ignore empty line
   std::getline(file, line);  // ignore "seed-to-soil map:" line
   while (std::getline(file, line)) {
      if (line.empty())
         continue;
      else if (line == "soil-to-fertilizer map:")
         soils.initialize(ranges);
      else if (line == "fertilizer-to-water map:")
         fertilizers.initialize(ranges);
      else if (line == "water-to-light map:")
         waters.initialize(ranges);
      else if (line == "light-to-temperature map:")
         lights.initialize(ranges);
      else if (line == "temperature-to-humidity map:")
         temperatures.initialize(ranges);
      else if (line == "humidity-to-location map:")
         humidities.initialize(ranges);
      else
         parse(ranges, line);
   }
   locations.initialize(ranges);

   const auto solve = [&]() {
      soils.transform(ranges);
      fertilizers.transform(ranges);
      waters.transform(ranges);
      lights.transform(ranges);
      temperatures.transform(ranges);
      humidities.transform(ranges);
      locations.transform(ranges);

      long min = std::numeric_limits<long>::max();
      for (Range& range : ranges)
         min = std::min(range.src, min);
      return min;
   };

   // --- Part One ---

   ranges.clear();
   for (long seed : seeds)
      ranges.emplace_back(Range(seed, -1, 1));
   std::cout << solve() << "\n";

   // --- Part Two ---

   ranges.clear();
   for (size_t i = 0; i < seeds.size() - 1; i += 2)
      ranges.emplace_back(Range(seeds[i], seeds[i], seeds[i + 1]));
   std::cout << solve() << "\n";

   return EXIT_SUCCESS;
}
