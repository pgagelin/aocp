# Advent of Code C++

Solutions to some [Advent of Code](https://adventofcode.com/) puzzles using C++ programming language.

It shows an example of CMake configuration with:

- compilation commands export
- tests
- dynamic analysis
- static analysis
- code coverage

For instance, a typical usage can be:

```sh
# Configure the build system with static analysis and coverage
cmake -B 0-build -D AOCP_ANALYSIS=ON -D AOCP_COVERAGE=ON .

# Build the libraries and binaries (static analysis occurs during this step)
make -C 0-build

# Run the tests
make -C 0-build test

# Run the tests with the dynamic analysis tool, here valgrind
ctest --test-dir 0-build -T memcheck

# Run the code coverage tool (tests must be run before)
ctest --test-dir 0-build -T coverage
```

## Advanced coverage report

The `ctest -T coverage` report generates an XML output report that can be parsed by [Coveralls](https://coveralls.io/). To generate a local HTML report after having run the tests, we can use the following commands:

```sh
# Capture source line hit from ".gcda" files under `0-build` directory
lcov --capture --directory 0-build --output-file 0-build/coverage.info

# Extract only our source code. If not, some code from libc++ will appear
lcov --extract 0-build/coverage.info "*aocp*" --output-file 0-build/coverage.info

# Generate an HTML report
genhtml 0-build/coverage.info --output-directory 0-build/coverage

# Open the report in your favorite web browser
browse 0-build/coverage/index.html
```

## Drawback

I can't use Clang/LLVM as compiler, it messes with LCOV. The issue seems to be about ".gcda" files version incompatibility. This is no real surprise as GCOV/LCOV are designed to work with GCC. Version conflict can appear in a distribution where Clang/LLVM and GCC are not synchronized. An alternative [LLVM source coverage](https://clang.llvm.org/docs/SourceBasedCodeCoverage.html) can be set up.

## Dependencies

The following packages are required:

- `clang-tidy`
- `cmake`
- `g++`
- `lcov`
- `valgrind`
