//
// Day 1: Historian Hysteria
//
// https://adventofcode.com/2024/day/1
//

#include <algorithm>
#include <fstream>
#include <iostream>
#include <unordered_map>
#include <vector>

int main(int argc, char **argv)
{
   std::ifstream file(argc == 2 ? argv[1] : "2024/day01/test01.txt");
   if (!file.is_open())
      return std::cerr << "Failed to open input file\n", EXIT_FAILURE;

   std::vector<int> left;
   std::vector<int> right;
   std::string line;
   while (std::getline(file, line))
      if (std::sscanf(line.c_str(), "%u   %u", &left.emplace_back(0), &right.emplace_back(0)) != 2)
         return std::cerr << "Failed to parse invalid input\n", EXIT_FAILURE;

   // --- Part One ---

   std::sort(left.begin(), left.end());
   std::sort(right.begin(), right.end());

   int distance = 0;
   for (std::size_t i = 0; i < right.size(); ++i)
      distance += std::abs(right[i] - left[i]);

   std::cout << distance << "\n";

   // --- Part Two ---

   std::unordered_map<int, int> occurences;
   for (const auto& i : right)
      occurences[i]++;

   int similarity = 0;
   for (const auto& i : left)
      similarity += i * occurences[i];

   std::cout << similarity << "\n";

   return EXIT_SUCCESS;
}
