//
// Day 18: RAM Run
//
// https://adventofcode.com/2024/day/18
//

#include <array>
#include <fstream>
#include <iostream>
#include <queue>
#include <vector>

struct Coord {
   bool operator<(const Coord& c) const { return x < c.x || (x == c.x && y < c.y); };
   unsigned col() const { return unsigned(x); }
   unsigned row() const { return unsigned(y); }
   int x;
   int y;
};

struct Path {
   Coord position;
   int length;
};

struct Memory {
   Memory(std::vector<Coord>&& w, size_t size)
      : bytes(std::move(w))
   {
      rows.resize(size);
      for (std::vector<char>& row : rows)
         row.resize(size, '.');
   }

   bool valid(const Coord& c) const { return c.x >= 0 && c.y >= 0 && c.col() < rows.size() && c.row() < rows.size() && rows[c.row()][c.col()] == '.'; }
   bool finish(const Coord& c) const { return c.col() + 1 == rows.size() && c.row() + 1 == rows.size(); }

   int path(unsigned fell)
   {
      // Fill memory
      for (auto& row : rows)
         std::fill(row.begin(), row.end(), '.');

      for (unsigned i = 0; i < fell; ++i)
         rows[bytes[i].row()][bytes[i].col()] = '#';

      // Breadth-first seach
      std::queue<Path> paths;
      std::vector<std::vector<bool>> visited(rows.size());
      for (auto& row : visited)
         row.resize(rows.size(), false);
      if (valid(Coord(0, 0))) {
         paths.push(Path(Coord(0, 0), 0));
         visited[0][0] = true;
      }
      while (!paths.empty()) {
         Path& path = paths.front();
         for (const Coord& d : directions) {
            Path current { Coord(path.position.x + d.x, path.position.y + d.y), path.length + 1 };
            if (finish(current.position))
               return current.length;
            if (valid(current.position) && !visited[current.position.row()][current.position.col()]) {
               visited[current.position.row()][current.position.col()] = true;
               paths.push(current);
            }
         }
         paths.pop();
      }

      return -1;
   }

   std::array<Coord, 4> directions {
      Coord { -1, 0  }, // left
      Coord { 0,  1  }, // up
      Coord { 1,  0  }, // right
      Coord { 0,  -1 }, // down
   };
   std::vector<std::vector<char>> rows;
   std::vector<Coord> bytes;
};

int main(int argc, char **argv)
{
   std::ifstream file(argc == 2 ? argv[1] : "2024/day18/test01.txt");
   if (!file.is_open())
      return std::cerr << "Failed to open input file\n", EXIT_FAILURE;

   std::vector<Coord> bytes;
   std::string line;
   while (std::getline(file, line)) {
      Coord& position = bytes.emplace_back(Coord {});
      if (std::sscanf(line.c_str(), "%u,%u", &position.x, &position.y) != 2)
         return std::cerr << "Failed to parse invalid input\n", EXIT_FAILURE;
   }
   const bool example = bytes.size() == 25;
   Memory grid(std::move(bytes), example ? 7 : 71);

   // --- Part One ---

   std::cout << grid.path(example ? 12 : 1024) << "\n";

   // --- Part Two ---

   // Bisect to reduce the number of checks
   unsigned ok = example ? 12 : 1024;
   unsigned ko = grid.bytes.size();
   while (ok + 1 != ko) {
      const unsigned fell = (ok + ko) / 2;
      if (grid.path(fell) != -1)
         ok = fell;
      else
         ko = fell;
   }
   std::cout << grid.bytes[ko - 1].x << ',' << grid.bytes[ko - 1].y << "\n";

   return EXIT_SUCCESS;
}
